from math import sqrt, log, pi, fabs, sin, cos
import numpy as np
from numpy import linalg

def orbital_velocity(u, r, a = -1):
    if a == -1:
        a = r
    return sqrt(u*(2/r - 1/a))

def orbital_period(u, a):
    return 2*pi*sqrt(a**3/u)



def eccentricity(periapsis, apoapsis):
    return (apoapsis - periapsis)/(apoapsis + periapsis)

def getAngle(a, b, distance):
    return

def chase_angle(a, b):
    return

#Assume circular start and final orbits
def hohmann_transfer(start, final):
    assert start.getEccentricity() < 0.05
    assert final.getEccentricity() < 0.05
    transferOrbit = Orbit(start.body, start.periapsis, final.apoapsis)
    first = fabs(transferOrbit.getVelocityAtPeri() - start.getVelocityAtPeri())
    second = fabs(final.getVelocityAtApo() - transferOrbit.getVelocityAtApo())
    return (transferOrbit, first, second)

#Assume circular orbits, TODO elliptic orbits
def plane_change(start, final):
    assert start.getEccentricity() < 0.05
    assert final.getEccentricity() < 0.05
    intersect = np.cross(start.getNormal(), final.getNormal())
    if linalg.norm(intersect) < 0.001:
        if linalg.norm(start.getNormal() - final.getNormal()) < 0.001:
            # Orbits are already perfectly aligned
            return 0
        else:
            # Orbits are aligned but reversed
            return 2*start.getVelocityAtPeri();
    else:
        # normalize the intersect vector
        intersect /= linalg.norm(intersect)
        return linalg.norm(np.cross(intersect, final.getPeriVector())*final.getVelocityAtPeri() - np.cross(intersect, start.getPeriVector())*start.getVelocityAtPeri())
        

class Orbit:
    def __init__(self, body, periapsis, apoapsis, inclination=0, argument=0):
        self.normal = np.array([sin(inclination)*cos(argument), sin(inclination)*sin(argument), cos(inclination)])
        self.perivector = np.array([cos(inclination)*cos(argument), cos(inclination)*sin(argument), -sin(inclination)])
        # The orbit should should sotre the inclination and argument
        # as the normal vector and vector towards periapsis
        if periapsis < apoapsis:
            self.periapsis = periapsis
            self.apoapsis = apoapsis
        else:
            self.apoapsis = periapsis
            self.periapsis = apoapsis
        self.body = body
        self.inclination = 0
        self.argument = argument;

    def getPeriod(self):
        return orbital_period(self.body.u, self.a)
    def getA(self):
        return self.body.radius + (self.apoapsis + self.periapsis)/2

    def getVelocityAt(self, altitude):
        return orbital_velocity(self.body.u, self.body.radius + altitude, self.getA())

    def getVelocityAtApo(self):
        return self.getVelocityAt(self.apoapsis)

    def getVelocityAtPeri(self):
        return self.getVelocityAt(self.periapsis)

    def getEccentricity(self):
        return eccentricity(self.periapsis, self.apoapsis)

    def getNormal(self):
        return self.normal

    def getPeriVector(self):
        return self.perivector

class Orbiter(Orbit):
    def __init__(self, body, periapsis, apoapsis, position=0, inclination=0, argument=0):
        Orbit.__init__(self, body, periapsis, apoapsis, inclination, argument)
        self.position = position

class Body(Orbiter):
    def __init__(self, parent, u, radius, periapsis, apoapsis, soi, position=0, inclination=0, argument=0):
        Orbiter.__init__(self, parent, periapsis, apoapsis, position, inclination)
        self.u = u
        self.radius = radius
        self.soi = soi


class Sun(Body):
    def __init__(self, u, radius):
        Orbiter.__init__(self, None, 0, 0, 0, 0)
    def getPeriod(self):
        return 0;

Kerbol = Sun(1.1723328e18, 261600000)

Kerbin = Body(Kerbol, 3.5616e12, 600000, 13599840256, 13599840256, 84159286)
Mun = Body(Kerbin, 6.5138398e10, 200000, 12000000, 12000000, 249559.1)
Minmus = Body(Kerbin, 1.7658e9, 60000, 47000000, 47000000, 2247428.4, 0, pi*6/180, pi*38/180)

LKO = Orbit(Kerbin, 80000, 80000)
MunOrbit = Orbit(Kerbin, 12000000, 12000000)
MunarTransfer = hohmann_transfer(LKO, MunOrbit)[0]

HSSOrbit = Orbit(Kerbin, 100074, 100587)
DescentFromHSS = hohmann_transfer(HSSOrbit, Orbit(Kerbin, 40000, 40000))[0]
HSS2Orbit = Orbit(Mun, 49828, 50135)
HSS2Polar = Orbit(Mun, 49828, 50135, pi/2)

if __name__ == "__main__":
    munnormal = Mun.getNormal()
    assert munnormal[0] == 0
    assert munnormal[1] == 0
    assert munnormal[2] == 1
    assert linalg.norm(Minmus.getNormal()) - 1 < 0.0001
    assert linalg.norm(Minmus.getPeriVector()) - 1 < 0.0001
    assert np.dot(Minmus.getNormal(), Minmus.getPeriVector()) < 0.0001
    LKOPolar = Orbit(Kerbin, 80000, 80000, pi/2)
    assert abs(plane_change(LKO, LKOPolar) - sqrt(2)*LKO.getVelocityAtPeri()) < 0.001
    LKOReverse = Orbit(Kerbin, 80000, 80000, pi)
    assert abs(plane_change(LKO, LKOReverse) - 2*LKO.getVelocityAtPeri()) < 0.001
    assert abs(plane_change(LKOReverse, LKOPolar) - sqrt(2)*LKOReverse.getVelocityAtPeri()) < 0.001
