from os import listdir
from os.path import isfile, join
from string import printable
import json

fuelTypes = {'LiquidFuel' : 0.005, 'Oxidizer' : 0.005, 'SolidFuel' : 0.0075, 'XenonGas' : 0.0001}

def putData(data, key, value):
	if key not in data:
		data[key] = []
	data[key].append(value)

def readDataBlock(file):
	data = {}
	oldLine = ""
	for line in file:
		line = "".join(c for c in line.strip() if c in printable)
		if line == "{":
			putData(data, oldLine, readDataBlock(file))
			continue
		elif line == "}":
			return data
		keyPair = [i.strip() for i in line.split('=')]
		if len(keyPair) == 2:
			putData(data, *keyPair)
		oldLine = line
	return data

def readCraft(fileName):
	fp = open(fileName)
	craftData = readDataBlock(fp)
	
	partData = {}
	for part in craftData['PART']:
		if 'link' not in part:
			part['link'] = []
		partData[part['part'][0]] = part['link']

	return partData, craftData['PART'][0]['part'][0]

def readPartConfig(fileName):
	fp = open(fileName)
	part = dict()
	partData = readDataBlock(fp)
	if "PART" in partData:
		partData = partData['PART'][0]
	part['name'] = partData['name'][0]
	part['dry_mass'] = float(partData['mass'][0])
	part['full_mass'] = part['dry_mass']
	part['title'] = partData['title'][0]
	part['decoupler'] = False
	if "MODULE" in partData:
		for module in partData['MODULE']:
			if 'name' in module and "Decouple" in module['name'][0]:
				part['decoupler'] = True
			if 'name' in module and "Engines" in module['name'][0]:
				part["engine"] = {}
				if 'maxThrust' in module:
					part["engine"]["thrust"] = float(module['maxThrust'][0])
				if 'atmosphereCurve' in module:
					for isp in module['atmosphereCurve'][0]['key']:
						values = isp.split(' ')
						if values[0] == '0':
							part["engine"]['isp_vac'] = int(values[1])
						if values[0] == '1':
							part["engine"]['isp_asl'] = int(values[1])
	if "RESOURCE" in partData:	
		part["fuel"] = {}
		for resource in partData['RESOURCE']:
			fuel = resource['name'][0]
			amount = float(resource['maxAmount'][0])
			print("%s fuelType %s amount %f" % (part['name'], fuel, amount))
			if fuel in fuelTypes:
				part["fuel"][fuel] = resource['maxAmount'][0]
				part['full_mass'] = part['full_mass'] + amount * fuelTypes[fuel] 
			if fuel == "MonoPropellant":
				part['full_mass'] = part['dry_mass'] + amount * (1.0 / 250.0)
		if len(part["fuel"]) == 0:
			del part["fuel"]
	return part

def printPartTree(parts, root, visited = [], level = 0):
	indent = " " * level
	if root not in visited:
		visited.append(root)
		print("%s%s" % (indent, root))
		for part in parts[root]:
			printPartTree(parts, part, visited, level + 1)
	else:
		print("%sDuplicate: %s" % (indent, root))
	return visited

def listPartConfigs(subdir):
	if 'Parts' in listdir(subdir):
		return listPartConfigs(join(subdir, 'Parts'))
	files = []
	for f in listdir(subdir):
		fileName = join(subdir, f)
		if isfile(fileName):
			if ".cfg" in fileName:
				files.append(fileName)
			else:
				continue
		else:
			files.extend(listPartConfigs(fileName))
	return files

def readKspParts(kspGameDataDir = "./"):
	partData = []
	configs = []
	for f in listdir(kspGameDataDir):
		fileName = join(kspGameDataDir, f)
		if isfile(fileName):
			continue
		configs.extend(listPartConfigs(fileName))
	for partConfig in configs:
		part = readPartConfig(partConfig)
		partData.append(part)
	return partData
