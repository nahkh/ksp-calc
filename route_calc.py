#Library for calculating missions starting from Kerbin

class Node:
    #Aerobreak key: 0 no aero, 1 possible from parent, -1 possible to parent
    def __init__(self, name, parent, dv, aerobreak=0, TWR_req = 0, max_plane=0, landable = False):
        self.name = name
        self.parent = parent
        if parent != None:
            self.parent.addChild(self)
        self.dv = dv
        self.aerobreak = aerobreak
        self.children = []
        self.TWR_req = TWR_req
        self.landable = landable
    def addChild(self, child):
        self.children.append(child)
    def getPathToRoot(self):
        parent = self.parent
        path = [self]
        while parent != None:
            path.insert(0, parent)
            parent = parent.parent
        return path
    def getNodeByName(self, name):
        if self.nameCompare(name):
            return self
        else:
            for child in self.children:
                value = child.getNodeByName(name)
                if value != None:
                    return value
            return None;    
    def nameCompare(self, name):
        return self.name.lower() == name.lower()
    def printSummary(self, indent = 0, dv = 0):
        print (" " * indent + self.name + " : %d"%(self.dv + dv))
        for child in self.children:
            child.printSummary(indent + 1, self.dv + dv)
    def calcDvTo(self, name, aerobreak = True):
        node = self.getNodeByName(name)
        path = node.getPathToRoot()
        return calc_dv_on_path(path, aerobreak)

    def calcDvFrom(self, name, aerobreak = True):
        node = self.getNodeByName(name)
        path = node.getPathToRoot()
        return calc_dv_on_path(path, aerobreak, -1)
    def getNodes(self):
        nodes = [self.name]
        for child in self.children:
            nodes.extend(child.getNodes())
        return nodes;
    
def calc_dv_on_path(path, aerobreak = True, direction = 1):
    dv = 0
    for step in path:
        if not aerobreak or (step.aerobreak == 0 or direction != step.aerobreak):
            dv += step.dv
    return dv
        

Kerbin = Node("Kerbin", None, 0, landable = True)
LowKerbinOrbit = Node("Low Kerbin Orbit", Kerbin, 4500, -1, 2)
KeoStationaryTransfer= Node("Keostationary Transfer", LowKerbinOrbit, 680, -1, 0)
KeoStationaryOrbit = Node("Keostationary Orbit", KeoStationaryTransfer, 435, 0, 0)
MunTransfer=Node("Mun Transfer", KeoStationaryTransfer, 180, -1, 0)
MunCapture=Node("Mun Capture", MunTransfer, 80, 0, 0)
LowMunOrbit = Node("Low Mun Orbit", MunCapture, 230, 0, 0)
Mun = Node("Mun", LowMunOrbit, 580, 0, 1, landable = True) #TWR guess
MinmusTransfer = Node("Minmus Transfer", MunTransfer, 70, -1, 0, 340)
MinmusCapture = Node("Minmus Capture", MinmusTransfer, 90, 0, 0)
LowMinmusOrbit = Node("Low Minmus Orbit", MinmusCapture, 70, 0, 0)
Minmus = Node("Minmus", LowMinmusOrbit, 180, 0, 0.5, landable = True) #TWR guess
KerbinEscape = Node("Kerbin Escape", MinmusTransfer, 90, -1, 0)
KerbinEveTransfer = Node("Kerbin-Eve Transfer", KerbinEscape, 90, -1, 0, 430)
EveCapture = Node("Eve Capture", KerbinEveTransfer, 80, 1, 0)
GillyTransfer = Node("Gilly Transfer", EveCapture, 60, 1, 0, 930)
LowEveOrbit = Node("Low Eve Orbit", GillyTransfer, 1270, 1, 0)
Eve = Node("Eve", LowEveOrbit, 12000, 1, 2, landable = True) #TWR guess
GillyCapture = Node("Gilly Capture", GillyTransfer, 400, 0, 0)
LowGillyOrbit = Node("Low Gilly Orbit", GillyCapture, 10, 0, 0)
Gilly = Node("Gilly", LowGillyOrbit, 30, 0, 0.1, landable = True) #TWR guess
KerbinMohoTransfer = Node("Kerbin-Moho Transfer", KerbinEveTransfer, 670, -1, 0, 2520)
MohoCapture = Node("Moho Capture", KerbinMohoTransfer, 2090, 0, 0)
LowMohoOrbit = Node("Low Moho Orbit", MohoCapture, 320, 0, 0)
Moho = Node("Moho", LowMohoOrbit, 870, 0, 1.5, landable = True) #TWR guess
KerbinKerbolTransfer = Node("Kerbin-Kerbol Transfer", KerbinMohoTransfer, 5150, -1, 0)
LowKerbolOrbit = Node("Low Kerbol Orbit", KerbinKerbolTransfer, 26820, 0, 0)
Kerbol = Node("Kerbol", LowKerbolOrbit, 66940, 0, 0)
KerbinDunaTransfer = Node("Kerbin-Duna Transfer", KerbinEscape, 130, -1, 0, 10)
KerbinDresTransfer = Node("Kerbin-Dres Transfer", KerbinDunaTransfer, 480, -1, 0, 1010)
KerbinJoolTransfer = Node("Kerbin-Jool Transfer", KerbinDresTransfer, 370, -1, 0, 270)
KerbinEelooTransfer= Node("Kerbin-Eeloo Transfer", KerbinJoolTransfer, 160, -1, 0, 1330)
KerbolEscape = Node("Kerbol Escape", KerbinEelooTransfer, 650, -1, 0)
EelooCapture = Node("Eeloo Capture", KerbinEelooTransfer, 1130, 0, 0)
LowEelooOrbit = Node("Low Eeloo Orbit", EelooCapture, 240, 0, 0)
Eeloo = Node("Eeloo", LowEelooOrbit, 620, 0, 1, landable = True) #TWR guess
JoolCapture = Node("Jool Capture", KerbinJoolTransfer, 160, 1, 0)
PolTransfer = Node("Pol Transfer", JoolCapture, 160, 1, 0, 700)
PolCapture = Node("Pol Capture", PolTransfer, 770, 0, 0)
LowPolOrbit = Node("Low Pol Orbit", PolCapture, 50, 0, 0)
Pol = Node("Pol", LowPolOrbit, 130, 0, 0.5, landable = True) #TWR guess
BopTransfer = Node("Bop Transfer", PolTransfer, 60, 1, 0, 2440)
BopCapture = Node("Bop Capture", BopTransfer, 830, 0, 0)
LowBopOrbit = Node("Low Bop Orbit", BopCapture, 70, 0, 0)
Bop = Node("Bop", LowBopOrbit, 220, 0, 0.5, landable = True) #TWR guess
TyloTransfer = Node("Tyle Transfer", BopTransfer, 180, 1, 0)
TyloCapture = Node("Tylo Capture", TyloTransfer, 240, 0, 0)
LowTyloOrbit = Node("Low Tylo Orbit", TyloCapture, 860, 0, 0)
Tylo = Node("Tylo", LowTyloOrbit, 2270, 0, 1, landable = True) #TWR guess
VallTransfer = Node("Vall Transfer", TyloTransfer, 220, 1, 0)
VallCapture = Node("Vall Capture", VallTransfer, 580, 0, 0)
LowVallOrbit = Node("Low Vall Orbit", VallCapture, 330, 0, 0)
Vall = Node("Vall", LowVallOrbit, 860, 0, 1, landable = True) #TWR guess
LaytheTransfer = Node("Laythe Transfer", VallTransfer, 310, 1, 0)
LaytheCapture = Node("Laythe Capture", LaytheTransfer, 290, 1, 0)
LowLaytheOrbit = Node("Low Laythe Orbit", LaytheCapture, 780, 1, 0)
Laythe = Node("Laythe", LowLaytheOrbit, 3200, 1, 2, landable = True) #TWR guess
LowJoolOrbit = Node("Low Jool Orbit", LaytheTransfer, 1880, 1, 0)
Jool = Node("Jool", LowJoolOrbit, 22000, 1, 5) #TWR guess
DresCapture = Node("Dres Capture", KerbinDresTransfer, 1140, 0, 0)
LowDresOrbit = Node("Low Dres Orbit", DresCapture, 150, 0, 0)
Dres = Node("Dres", LowDresOrbit, 430, 0, 1, landable = True) #TWR guess
DunaCapture = Node("Duna Capture", KerbinDunaTransfer, 250, 1, 0)
IkeTransfer = Node("Ike Transfer", DunaCapture, 30, 1, 0)
IkeCapture = Node("Ike Capture", IkeTransfer, 30, 0, 0)
LowIkeOrbit = Node("Low Ike Orbit", IkeCapture, 150, 0, 0)
Ike = Node("Ike", LowIkeOrbit, 390, 0, 0.5, landable = True) #TWR guess
LowDunaOrbit = Node("Low Duna Orbit", IkeTransfer, 330, 1, 0)
Duna = Node("Duna", LowDunaOrbit, 1300, 1, 2, landable = True) #TWR guess

allValidPaths= Kerbin.getNodes()

def estimate_dv(target, origin = "Kerbin", aerobreak = True, twoway = False):
    originPath = Kerbin.getNodeByName(origin).getPathToRoot()
    targetPath = Kerbin.getNodeByName(target).getPathToRoot()
    while(len(originPath) > 0 and len(targetPath) > 0 and originPath[0].name == targetPath[0].name):
        originPath.pop(0);
        targetPath.pop(0);
    one_way = calc_dv_on_path(targetPath, aerobreak, 1) + calc_dv_on_path(originPath, aerobreak, -1)
    if twoway:
        return_trip = calc_dv_on_path(targetPath, aerobreak, -1) + calc_dv_on_path(originPath, aerobreak, 1)
        return one_way + return_trip
    else:
        return one_way

def get_valid_nodes():
    return allValidPaths

#TODO A more sophisticated version that takes a ship assembly as a parameter
def get_routes(dv, origin="Kerbin", twoway = True, land = False):
    valid = []
    for destination in allValidPaths:
        if(dv > estimate_dv(destination, origin, True, twoway) and (not land or Kerbin.getNodeByName(destination).landable)):
            valid.append(destination)
    return valid

if __name__=="__main__":
    # unit tests
    assert len(get_valid_nodes()) == 69
    assert estimate_dv("mun") == 4500 + 680 + 180 + 80 + 230 + 580
    assert estimate_dv("kerbin", "mun") == 580 + 230 + 80
    assert estimate_dv("mun", "minmus", False) == 180 + 70 + 90 + 70 + 80 + 230 + 580
    assert estimate_dv("mun", "minmus") == 180 + 70 + 90 + 80 + 230 + 580
    assert estimate_dv("mun", twoway = True) == 4500 + 680+ 180 + (80+230+580)*2
