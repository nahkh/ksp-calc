import orbital_calc
import stage_calc
import numpy as np

def getDV(params):
    return stage_calc.dv(params["isp"], params["mass_full"], params["mass_dry"])

def TWR(params):
    return stage_calc.TWR(params["g"], params["thrust"], params["mass_full"], params["mass_dry"])
