import ksp_db, craft_parser 
from ksp_components import Engine, Tank, Decoupler
from ksp_stage import StageAssembly
from os.path import join

def printComponent(component, indent = ""):
	print("%sComponent %s" % (indent, component.getName()))
	multiLine = False
	if issubclass(component.__class__, Engine):
		multiLine = True
		print("%sThrust %f, ISP(ASL) %f, ISP(VAC) %f" % (indent, component.getThrust(), component.getIspAsl(), component.getIspVac()))
	if issubclass(component.__class__, Tank):
		multiLine = True
		print("%s%s" % (indent, str(component.getFuelConfig())))
	if issubclass(component.__class__, Decoupler):
		multiLine = True
		print("%sIs a decoupler" % (indent))
	print("%sDry mass %f" % (indent, component.getMass()))
	if multiLine:
		print()

def getStage(partList, root):
	stage = [ksp_db.getComponentFromDB(root.split('_')[0])]
	subStage = []
	stack = partList[root]
	while len(stack) > 0:
		i = stack.pop(0)
		component = ksp_db.getComponentFromDB(i.split('_')[0])
#		printComponent(component)
		if issubclass(component.__class__, Decoupler):
			subStage.append(i)
			continue
		else:
			stage.append(component)
		stack.extend(partList[i])
	result = [stage]
	for i in subStage:
		sub = getStage(partList, i)
		if len(result) == 1:
			result.extend(sub)
		else:
			for x in range(0, len(sub)):
				result[x+1].extend(sub[x])
	return result

def printStage(stageList, level = 0):
	indent = "  " * level
	print("%sStage %d" % (indent, level))
	for i in stageList:
		if type(i) is list:
			printStage(i, level + 1)
		else:
			printComponent(i, indent)

def readStageAssemblyFromCraft(path, assemblyName):
	(partList, root) = craft_parser.readCraft(join(path, "%s.craft" % assemblyName))
	stageAssembly = StageAssembly(assemblyName)
	for i in getStage(partList, root):
		stageAssembly.addStage(i)
	return stageAssembly

if __name__ == "__main__":
	readStageAssemblyFromCraft("./", "Herbic Space Station").print()
