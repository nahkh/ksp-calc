from orbital_calc import *
from stage_calc import *

class Step:
    def __init__(self, name, dv):
        self.name = name
        self.dv = dv

launchStep = Step("Launch", 4550)

class Mission:
    width = [20,8,8]
    def __init__(self, name):
        self.name = name
        self.steps=[]
    def add_step(self, step):
        self.steps.append(step)
    def printMission(self):
        print("\nMission %s"%self.name)
        printJustified(Mission.width, "Step", "dv","cumulative dv")
        total_dv = 0
        for step in self.steps:
            total_dv += step.dv
            printJustified(Mission.width, step.name, "%1.1f"%step.dv, "%1.1f"%total_dv)
        printJustified(Mission.width, "Total dv required", "%1.1f"%total_dv)

def printJustified(width, *strings):
    if isinstance(width, list):
       widths = width
    else:
        widths = [width] * len(strings)
    text = ""
    for i in range(0, len(strings)):
        text += strings[i].ljust(widths[i])
    print(text)
