import sqlite3, sys, ksp_components as comp
from os.path import exists
from craft_parser import readKspParts

specialComponents = ["sepMotor1"]
dbFile = "ksp_parts.db"

def createDB(kspGameDataDir, dbfile):
	if exists(dbFile):
		print("DB Already Exists. Move Along");
		return

	conn = sqlite3.connect(dbFile)
	cur = conn.cursor()
	cur.execute('''CREATE TABLE Part (
	 craftName varchar primary key,
	 titleName varchar unique not null,
	 dryMass numeric not null,
	 fullMass numeric not null
	);''')
	cur.execute('''CREATE TABLE Engine (
	 craftName varchar,
	 thrust numeric not null,
	 ispAsl numeric not null,
	 ispVac numeric not null,
	 foreign key(craftName) references Part
	 primary key(craftName)
	);''')
	cur.execute('''CREATE TABLE Tank (
	 craftName varchar,
	 fuelType varchar not null,
	 amount numeric not null,
	 foreign key(craftName) references Part,
	 primary key(craftName, fuelType)
	);''')
	cur.execute('''CREATE TABLE Decoupler (
	 craftName varchar,
	 foreign key(craftName) references Part,
	 primary key(craftName)
	);''')

	for part in readKspParts(kspGameDataDir):
		baseData = [part['name'], part['title'], part['dry_mass'], part['full_mass']]
		cur.execute("insert into Part (craftName, titleName, dryMass, fullMass) values (?, ?, ?, ?)", baseData)
		if part['name'] in specialComponents:
			continue
		if "engine" in part:
			engine = part["engine"]
			engineData = [part['name'], engine['thrust']]
			if 'isp_asl' in engine: 
				engineData.append(engine['isp_asl'])
			else:
				engineData.append(0)
			if 'isp_vac' in engine:
				engineData.append(engine['isp_vac'])
			else:
				engineData.append(0)
			cur.execute("insert into Engine (craftName, thrust, ispasl, ispvac) values (?,?,?,?)", engineData)
		if "fuel" in part:
			fuel = part["fuel"]
			for i in fuel:
				fuelData = [part['name'], i, fuel[i]]
				cur.execute("insert into Tank (craftName, fuelType, amount) values (?,?,?)", fuelData)
		if part['decoupler']:
			cur.execute("insert into Decoupler (craftName) values (?)", [part['name']])
	conn.commit()
	cur.close()

def execute(cur, query, params = []):
	cur.execute(query, params)
	results = []
	for i in cur:
		results.append(list(i))
	cur.close()
	return results

def getComponentFromDB(craftName):
	craftName = craftName.replace('.', '_')
	conn = sqlite3.connect(dbFile)
	cur = conn.cursor()
	results = execute(cur, "select titleName, dryMass, fullMass from part where craftName = ?", [craftName])
	if len(results) == 0:
		raise Exception("Craft with name %s was not found" % (craftName))
	i = results[0]
	part = {}
	part['name'] = i[0]
	part['dry_mass'] = i[1]
	part['full_mass'] = i[2]
	part.update(getEngineProperties(craftName, conn))
	part.update(getTankProperties(craftName, conn))
	decoupler = isDecoupler(craftName, conn)
	cur.close()
	classes = []
	if "engine" in part:
		classes.append(comp.Engine)
	if "fuel" in part:
		classes.append(comp.Tank)
	if decoupler:
		classes.append(comp.Decoupler)
	if len(classes) == 0:
		classes.append(comp.Component)
	ac = type("ActualComponet", tuple(classes), part)
	return ac.__new__(ac)


def getEngineProperties(craftName, dbconn):
	cur = dbconn.cursor()
	results = execute(cur, "select thrust, ispasl, ispvac from engine where craftName = ?", [craftName])
	if len(results) != 1:
		return {}
	i = results[0]
	cur.close()
	return {"engine" : {'thrust' : i[0], 'isp_asl' : i[1], 'isp_vac' : i[2]}}

def getTankProperties(craftName, dbconn):
	cur = dbconn.cursor()
	results = execute(cur, "select fuelType, amount from tank where craftName = ?", [craftName])
	if len(results) == 0:
		return {}
	fuel = {}
	for row in results:
		i = list(row)
		fuel[i[0]] = i[1]
	return {"fuel" : fuel, 'fill_rate' : 1}	

def isDecoupler(craftName, dbconn):
	cur = dbconn.cursor()
	results = execute(cur, "select * from decoupler where craftName = ?", [craftName])
	return len(results) == 1

if __name__ == "__main__":
	kspGameDataDir = "/Users/teemusa/Library/Application Support/Steam/SteamApps/common/Kerbal Space Program/GameData"
	createDB(kspGameDataDir, dbFile)
