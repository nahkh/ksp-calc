from orbital_calc import *
from stage_calc import *
from mission_planner import *

munLanding = Mission("Simple Mun landing")
munLanding.add_step(Step("Aerobrake", 0))
munLanding.add_step(Step("Mun escape",230+80))
munLanding.add_step(Step("Launch from Mun",580))
munLanding.add_step(Step("Landing to Mun",580))
munLanding.add_step(Step("Mun insertion",230+80))
munLanding.add_step(Step("Kerbin insertion",680+180))
munLanding.add_step(launchStep)
munLanding.printMission()

munPolar = plane_change(HSS2Orbit, HSS2Polar)

munLanding = Mission("Munar excursion")
munLanding.add_step(Step("Max plane change", munPolar))
munLanding.add_step(Step("Launch from Mun",580))
munLanding.add_step(Step("Landing to Mun",580))
munLanding.add_step(Step("Max plane change", munPolar))
munLanding.printMission()

orbitOperations = Mission("Orbital operations near Kerbin")
orbitOperations.add_step(Step("Landing from HSS", HSSOrbit.getVelocityAtPeri() - DescentFromHSS.getVelocityAtApo()))
orbitOperations.printMission()

ship1 = StageAssembly("Fuel to LKO")
ship1.addStage(1.1, PlaceAnywhere, MonoTank, enginecount=2)
ship1.addStage(7.1, LV1R, FLT800, enginecount=2)
ship1.addStage(17.4, LVT30, [FLT800, FLT800])
ship1.addStage(104.9, S1SRB(0.65, 1), {}, 4)
ship1.printAssemblyInfo()


ship2 = StageAssembly("MEM")
ship2.addStage(3.8, RM2477, FLT400, enginecount=2)
ship2.addStage(20, LVNAtomic, [FLT800, FLT800])
ship2.addStage(98.2, Mainsail, [Jumbo64,Jumbo64])
ship2.addStage(185.4, S1SRB, {}, 4)
ship2.printAssemblyInfo(9.81, True)

munShuttle = Mission("Mun shuttle and return")
munShuttle.add_step(Step("Kerbin return (aerobreak?)", 680+180))
munShuttle.add_step(Step("Mun escape",230+80))
munShuttle.add_step(Step("Mun insertion",230+80))
munShuttle.add_step(Step("Kerbin insertion",680+180))
munShuttle.printMission()

ship3 = StageAssembly("Scruffy")
ship3.addStage(6.6, LVNAtomic, [FLT200])
ship3.addStage(16.5, LVNAtomic, [FLT400])
ship3.addStage(94.7, Mainsail, [Jumbo64,Jumbo64])
ship3.addStage(183.1, S1SRB, {}, 4)
ship3.printAssemblyInfo()
