from math import log
from numbers import Number

def TWR(g, thrust, mass_full, mass_dry):
    return (thrust / (g * mass_full), thrust / (g * mass_dry))

def dv(isp, mass_full, mass_dry):
    assert mass_full > 0
    assert mass_dry > 0
    return isp * 9.81 * log(mass_full/mass_dry)

class Component:
    def __init__(self, mass):
        self.mass = mass
    def getMass(self):
        return self.mass

class Engine(Component):
    def __init__(self, mass, thrust, isp_asl, isp_vac):
        self.mass = mass
        self.thrust = thrust
        self.isp_asl = isp_asl
        self.isp_vac = isp_vac
    def getEngineMass(self):
        return self.mass

class Tank(Component):
    def __init__(self, mass, mass_fuel):
        self.mass = mass
        self.mass_fuel = mass_fuel
    def getTankDryMass(self):
        return self.mass;
    def __call__(self, fuel_percentage):
        return Tank(self.mass, self.mass_fuel*fuel_percentage)

class Booster(Engine, Tank):
    def __init__(self, mass, mass_fuel, thrust, isp_asl, isp_vac):
        self.mass_fuel = mass_fuel
        self.mass = mass
        self.thrust = thrust
        self.isp_asl = isp_asl
        self.isp_vac = isp_vac

    def __call__(self, thrust_limit, fuel_percentage):
        return Booster(self.mass, self.mass_fuel*fuel_percentage, self.thrust*thrust_limit, self.isp_asl, self.isp_vac)
    
    def getEngineMass(self):
        return 0;
        
class Stage:
    def __init__(self, mass, engine, enginecount, tanks):
        self.engine = engine
        self.enginecount = enginecount
        self.fullmass = mass
        self.drymass = mass
        for tank in tanks:
            amount = tanks[tank]
            self.drymass -= tank.mass_fuel * amount
    def getTWR(self, g, payload):
        return TWR(g, self.engine.thrust * self.enginecount, self.fullmass + payload, self.drymass + payload)
    def getDv(self, payload, atmo=False, fuel=1):
        if atmo:
            return dv(self.engine.isp_asl, (self.fullmass * fuel + self.drymass * (1-fuel)) + payload, self.drymass + payload)
        else:
            return dv(self.engine.isp_vac, (self.fullmass * fuel + self.drymass * (1-fuel)) + payload, self.drymass + payload)
    def getMass(self):
        return self.fullmass;
    
class StageAssembly:

    #Should be in mission planner
    dv_to_orbit = 4550
    min_twr_for_ascent = 1.5
    high_twr_threshold = 5
    
    def __init__(self, name):
        self.stages = []
        self.name = name

    def addStage(self, mass, engine, tanks, enginecount=1):
        mass = mass - self.getPayloadMass();
        if isinstance(tanks, Tank):
            tanks = {tanks: 1}
        if isinstance(tanks, list):
            temp_tanks = {}
            for tank in tanks:
                if tank in temp_tanks:
                    temp_tanks[tank] += 1
                else:
                    temp_tanks[tank] = 1
            tanks = temp_tanks
        if isinstance(engine, Booster):
            if engine in tanks:
                tanks[engine] = tanks[engine] + enginecount
            else:
                tanks[engine] = enginecount
        newStage = Stage(mass, engine, enginecount, tanks)
        self.stages.append(newStage)

    def getCurrentMass(self):
        if(len(self.stages) == 0):
            return 0
        else:
            return self.stages[-1].getCurrentMass()

    def getPayloadMass(self, stage=-1):
        if stage == -1:
            stage = len(self.stages)
        mass = 0
        for i in range(0, stage):
            mass += self.stages[i].getMass()
        return mass

    def printAssemblyInfo(self, g = 9.81, ascent = True, from_stage = -1):
        if from_stage < 0:
            stageindex = len(self.stages)-1
        else:
            stageindex = min(len(self.stages)-1, from_stage)
        print("\nStage assembly %s:\n"%self.name)
        print("Stage\tMass\tdv(asl)\tdv(vac)\tdv(cum)\tTWR min\tTWR max")
        deltav_count = 0;
        while(stageindex >= 0):
            stage = self.stages[stageindex]
            payload = self.getPayloadMass(stageindex)
            (twr_min, twr_max) = stage.getTWR(g, payload)
            dv_asl = stage.getDv(payload, True)
            dv_vac = stage.getDv(payload, False)
            warnings = ""
            if(ascent and deltav_count < self.dv_to_orbit and twr_min < self.min_twr_for_ascent):
                warnings = "Warning, low TWR"
            if(twr_max > self.high_twr_threshold):
                warnings = "Warning, very high TWR"
            if(ascent and deltav_count < self.dv_to_orbit):
                deltav_count += dv_asl
            else:
                deltav_count += dv_vac
            print("%d\t%1.1f\t%1.1f\t%1.1f\t%1.2f\t%1.2f\t%1.2f\t%s"%(stageindex, payload+stage.getMass(), dv_asl, dv_vac, deltav_count, twr_min, twr_max, warnings) )
            
            stageindex -= 1
        print ("Estimated total dv = %f\n"%(deltav_count,));
        
    def getDv(self, stageindex=-1, fuel=1):
        if stageindex == -1:
            stageindex = len(self.stages)
        else:
            stageindex = stageindex + 1
        total_dv = 0
        for i in range(0, stageindex):
            if i == stageindex - 1:
                total_dv += self.stages[i].getDv(self.getPayloadMass(i), False, fuel)
            else:
                total_dv += self.stages[i].getDv(self.getPayloadMass(i))
        return total_dv
        
        
FLT100 = Tank(0.0725, 0.49)
FLT200 = Tank(0.125, 1)
FLT400 = Tank(0.25, 2)
FLT800 = Tank(0.5, 4)
X20032 = Tank(2, 16)
Jumbo64 = Tank(4, 32)
BACC = Booster(1.505, 6.37, 315, 230, 250)
RT10 = Booster(3.7475 - 3.25, 3.25, 250, 225, 240)
S1SRB = Booster(3, 18.75,650, 230, 250)
LV909 = Engine(0.5, 50, 300, 390)
LVT30 = Engine(1.25, 215, 320, 370)
LVT45 = Engine(1.5, 200, 320, 370)
LVNAtomic = Engine(2.25, 60, 220, 800)
Skipper = Engine(3.0, 650, 320, 370)
Mainsail = Engine(6.0, 1500, 320, 360)
LFB = Booster(42-32, 32, 2000, 290, 340)
LV1R = Engine(0.03, 4.0, 220, 290)
PlaceAnywhere = Engine(0.05, 1, 100, 260)
MonoTank = Tank(0, 0.04)
RM2477 = Engine(0.09, 20, 250, 300)

TriCoupler = Component(0.15)
StackDecoupler = Component(0.05)
RadialDecoupler = Component(0.05)
NoseCone = Component(0.03)
Winglet = Component(0.02)
CommandPod1 = Component(0.84)
CommandPod12 = Component(4.12)
MobileProcessingLab = Component(3.5)
DockingPort = Component(0.05)
ShieldedDockingPort = Component(0.1)
OKTO = Component(0.1)
HECS = Component(0.1)
Stayputnik = Component(0.05)
Communotron16 = Component(0.005)
CommDTS = Component(0.03)
Z200 = Component(0.01)
LT1LandingStruts = Component(0.05)
Mk16Parachute = Component(0.1)
Mk2RadialParachute = Component(0.15)
PhotovoltaicPanel = Component(0.005)
LargeWheel = Component(0.2)
RCSBlock = Component(0.05)
StratusV = Component(0.75)
AdvancedInlineStabilizer = Component(0.1)
RockomaxBrandAdapter = Component(0.1)



#Run tests
if __name__=="__main__":
    ship1 = StageAssembly("Ship 1")
    ship1.addStage(3.84, LV909, {FLT200:1})
    ship1.addStage(10.14, LVT45, {FLT800:1})
    ship1.addStage(34.025, BACC(0.6, 1), {}, 3)
    assert ship1.getPayloadMass(0) == 0
    assert ship1.getPayloadMass(1) == 3.84
    assert ship1.getPayloadMass(2) == 10.14
    assert abs(ship1.getDv() - 4997.7) < 0.1
    assert abs(ship1.getDv(0) - 1154.2) < 0.1
    assert abs(ship1.getDv(0, 0.5) - 620.4) < 0.1

    ship2 = StageAssembly("Test 1")
    ship2.addStage(1+1.25+2.25, LVT30, {FLT200:2})
    #ship2.printAssemblyInfo()
    ship3 = StageAssembly("Test 2")
    ship3.addStage(1+1.25+2.25, LVT30, [FLT200, FLT200])
    #ship3.printAssemblyInfo()
    ship4 = StageAssembly("Test 3")
    ship4.addStage(1+1.25+2.25, LVT30, FLT400)
    #ship4.printAssemblyInfo()
    assert ship2.getDv() > 100
    assert abs(ship2.getDv() - ship3.getDv()) < 0.01
    assert abs(ship2.getDv() - ship4.getDv()) < 0.01
