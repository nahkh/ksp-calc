from orbital_calc import *
from stage_calc import *
from mission_planner import *
        
tutorial = Mission("Get to low Kerbin orbit")
tutorial.add_step(Step("Reentry", 50))
tutorial.add_step(launchStep)
tutorial.printMission()

ship1 = StageAssembly("Ship 1")
ship1.addStage(3.84, LV909, FLT200)
ship1.addStage(9.9, LVT45, FLT800)
ship1.addStage(33.8, BACC(0.6, 1), {}, 3)
ship1.printAssemblyInfo()


munFlyby = Mission("Munar flyby")
(transfer, first, second) = hohmann_transfer(LKO, MunOrbit)
munFlyby.add_step(Step("Return", 200))
munFlyby.add_step(Step("Mun transfer", first))
munFlyby.add_step(launchStep)
munFlyby.printMission()

ship2 = StageAssembly("Ship 2")
ship2.addStage(3.84, LV909, FLT200)
ship2.addStage(9.9, LVT45, [FLT800, FLT400])
ship2.addStage(33.8, BACC(0.6, 1), {}, 3)
ship2.printAssemblyInfo()

munLanding = Mission("Mun landing")
munLanding.add_step(Step("Aerobrake", 0))
munLanding.add_step(Step("Mun escape",230+80))
munLanding.add_step(Step("Launch from Mun",580))
munLanding.add_step(Step("Landing to Mun",580))
munLanding.add_step(Step("Mun insertion",230+80))
munLanding.add_step(Step("Kerbin insertion",680+180))
munLanding.add_step(launchStep)
munLanding.printMission()
