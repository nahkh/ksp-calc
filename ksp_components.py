class Component:
	def __init__(self, dry_mass = 0, full_mass = 0, name = ""):
		self.dry_mass = dry_mass
		self.full_mass = full_mass
		self.name = name

	def getMass(self):
		return self.full_mass

	def getName(self):
		return self.name

class Tank(Component):
	def __init__(self, fuel = {}, fill_rate = 1):
		self.fuel = fuel
		self.fill_rate = fill_rate

	def getFuelMass(self):
		return (self.full_mass - self.dry_mass) * self.fill_rate

	def getMass(self):
		return self.dry_mass

	def getFuelTypes(self):
		return list(self.fuel.keys())

	def getFuelConfig(self):
		fuelConfig = {}
		for type in self.fuel:
			fuelConfig[type] = self.fuel[type] * self.fill_rate
		return fuelConfig

	def __call__(self, fillRate):
		if fillRate < 0 or fillRate > 1:
			raise Exception("Fill rate needs to be between 0 and 1")
		self.fill_rate = fillRate

	def getFullMass(self):
		return self.dry_mass + self.getFuelMass()

class Engine(Component):
	def __init__(self, engine = { "thrust" : 0, "isp_asl" : 0, "isp_vac" : 0}):
		super().__init__()
		self.engine = engine

	def getThrust(self):
		return self.engine['thrust']

	def getIspAsl(self):
		return self.engine['isp_asl']

	def getIspVac(self):
		return self.engine['isp_vac']

class Decoupler(Component):
	pass
