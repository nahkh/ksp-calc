from math import log
from numbers import Number
from ksp_components import Engine, Tank

def TWR(g, thrust, mass_full, mass_dry):
    return (thrust / (g * mass_full), thrust / (g * mass_dry))

def dv(isp, mass_full, mass_dry):
    assert mass_full > 0
    assert mass_dry > 0
    return isp * 9.81 * log(mass_full/mass_dry)

class Stage:
    def __init__(self, components):
        self.engines = []
        self.tanks = []
        self.components = components
        self.drymass = 0
        for i in components:
            self.drymass = self.drymass + i.getMass()
            if issubclass(i.__class__, Engine):
                self.engines.append(i)
            if issubclass(i.__class__, Tank):
                self.tanks.append(i)

    def getTWR(self, g, payload):
        if len(self.engines) == 0:
            return (0, 0)
        return TWR(g, self.engines[0].getThrust() * len(self.engines), self.getMass() + payload, self.drymass + payload)

    def getDv(self, payload, atmo=False, fuel=1):
        if len(self.engines) == 0:
            return 0
        if fuel >= 0 and fuel < 1:
            for tank in self.tanks:
                tank(fuel)
        if atmo:
            return dv(self.engines[0].getIspAsl(), self.getMass() + payload, self.drymass + payload)
        else:
            return dv(self.engines[0].getIspVac(), self.getMass() + payload, self.drymass + payload)

    def getTanks(self):
        return self.tanks

    def getMass(self):
        return self.drymass + self.getFuelMass()

    def getFuelMass(self):
        mass = 0
        for tank in self.tanks:
            mass = mass + tank.getFuelMass()
        return mass        
    
class StageAssembly:

    #Should be in mission planner
    dv_to_orbit = 4550
    min_twr_for_ascent = 1.5
    high_twr_threshold = 5
    
    def __init__(self, name):
        self.stages = []
        self.name = name

    def addStage(self, components):
        newStage = Stage(components)
        self.stages.append(newStage)

    def getCurrentMass(self):
        mass = 0
        for stage in self.stages:
            mass = mass + stage.getMass()
        return mass

    def getPayloadMass(self):
        return getPayloadMass(self, len(self.stages))

    def getPayloadMass(self, stage):
        mass = 0
        for i in self.stages[:stage]:
            mass += i.getMass()
        return mass

    def getStageStats(self, g = 9.81):
        stageStats = []
        payload = 0
        for stage in self.stages:
            stats = {}
            (twr_min, twr_max) = stage.getTWR(g, payload)
            stats['twr_min'] = twr_min
            stats['twr_max'] = twr_max
            stats['dv_asl'] = stage.getDv(payload, True)
            stats['dv_vac'] = stage.getDv(payload, False)
            stats['full_mass'] = payload + stage.getMass()
            stats['dry_mass'] = stats['full_mass'] - stage.getFuelMass()
            stageStats.append(stats)
            payload = payload + stage.getMass()
        return stageStats[::-1]

    def print(self, g = 9.81, ascent = True, from_stage = -1):
        if from_stage == -1:
            from_stage = len(self.stages)
        else:
            from_stage = min(len(self.stages), from_stage)
        print("\nStage assembly %s:\n"%self.name)
        print("Stage\tFull\tDry\tdv(asl)\tdv(vac)\tdv(cum)\tTWR min\tTWR max")
        
        deltav_count = 0
        index = from_stage - 1
        for stats in self.getStageStats(g)[:from_stage]:
            warnings = ""
            if(ascent and deltav_count < self.dv_to_orbit and stats['twr_min'] < self.min_twr_for_ascent):
                warnings = "Warning, low TWR"
            if(stats['twr_max'] > self.high_twr_threshold):
                warnings = "Warning, very high TWR"
            if(ascent and deltav_count < self.dv_to_orbit):
                deltav_count += stats['dv_asl']
            else:
                deltav_count += stats['dv_vac']
            fullMass = stats['full_mass']
            dryMass = stats['dry_mass'] 
            print("%d\t%1.1f\t%1.1f\t%1.1f\t%1.1f\t%1.2f\t%1.2f\t%1.2f\t%s"%(index, fullMass, dryMass, stats['dv_asl'], stats['dv_vac'], deltav_count, stats['twr_min'], stats['twr_max'], warnings) )
            index = index - 1
        print ("Estimated total dv = %f\n"%(deltav_count,));
        
    def getDv(self, stageindex=-1, fuel=1):
        if stageIndex == -1:
            stageIndex = len(self.stages)
        total_dv = 0
        for i in range(0, len(self.stages)):
            if i == stageindex - 1:
                total_dv += self.stages[i].getDv(self.getPayloadMass(i), False, fuel)
            else:
                total_dv += self.stages[i].getDv(self.getPayloadMass(i))
        return total_dv
